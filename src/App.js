import './App.scss';
import { Route, BrowserRouter, Routes } from 'react-router-dom';

import Home from './pages/Home';

import Equipe from './pages/Equipe';
import Competence from './pages/Competence';
import Service from './pages/Service';
import About from './pages/About';
import Contact from './pages/Contact';

import SearchBar from "../src/components/SearchBar";
import BurgerMenu from "../src/components/BurgerMenu";

import React, { useState } from 'react';

function App() {

  const [menuOpen, setMenuOpen] = useState(false);
    
  const handleClick = () => {
      setMenuOpen(!menuOpen);
      console.log(menuOpen)
  };

  return (
    <BrowserRouter>
      <div className="nav">
        <div className="left">
            <BurgerMenu onClick={handleClick}/>
            <SearchBar />
        </div>
        <div className="right">
            <img src="./logo-header.png" alt="logo" className="logo-header"></img>
        </div>
      </div>
      <Routes>
        <Route exact path="/" element={<Home menu={menuOpen} handleClick={handleClick}/>} />
        <Route path="/competence" element={<Competence/>} />
        <Route path="/service" element={<Service/>} />
        <Route path="/a-propos" element={<About/>} />
        <Route path="/equipe" element={<Equipe/>} />
        <Route path="/nous-contacter" element={<Contact/>} />
      </Routes>
      
    </BrowserRouter>
  );
}


export default App;
