import { Link } from "react-router-dom";

export default function Hero() {
    return(
        <div className="container-hero">
            <Link id="share-box">
                <div className="share-box">
                    <img src="./assets/icons/instagram.svg" alt="instagram icon"/>
                    <h3>Follow us on Instagram</h3>
                </div>
            </Link>
            <div className="hero-title">
                <h1>
                    Sept pour Cent 
                    -
                    7%
                </h1>
                <h2>Agence Web</h2>

            </div>
            <div className="hero-description">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>
            <div className="hero-buttons">
                <div className="hero-button hero-button-1">
                    <img src="./assets/icons/contact.svg" alt="nous contacter"/>
                    <Link className="hero-button-link" to="/">Nous contacter</Link>
                </div>
                <div className="hero-button hero-button-2">
                    <img src="./assets/icons/team.svg" alt="notre équipe"/>
                    <Link className="hero-button-link" to="/">Notre équipe</Link>
                </div>
            </div>
        </div>
    )
}