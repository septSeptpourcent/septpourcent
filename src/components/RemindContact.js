import { Link } from "react-router-dom";
import "../styles/components/RemindContact.scss"

export default function RemindContact(){
    return(
        <section className="section6">
            <div className="container-remindContact">
                <div className="left">
                    <h4>Contactez-nous pour votre projet !</h4>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing 
                        and typesetting industry. 
                    </p>
                    <Link to="/nous-contacter" className="link-remindcontact">
                        <span>Contactez-nous</span>
                    </Link>
                </div>
                <div className="right">
                    <img src="./team_up.svg" alt="illustration" />
                </div>
            </div>
        </section>
    )
}