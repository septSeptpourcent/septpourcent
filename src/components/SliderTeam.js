import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Scrollbar } from 'swiper';
import 'swiper/css/navigation';
import 'swiper/css/scrollbar';
import 'swiper/css';

export default function SliderTeam(){
    return(
        <Swiper
            spaceBetween={10}
            slidesPerView={4}
            loop
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
        >
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
            <SwiperSlide>
                <div>
                    <img src="./assets/team.svg" alt='team' className='slider-team'/>
                </div>
            </SwiperSlide>
        </Swiper>
    )
}