import "../styles/burger-button.scss";
import { useState } from "react";

export default function BurgerMenu({ onClick }) {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = () => {
    setIsOpen(!isOpen);
    onClick();
  };

  return (
    <div className={`menu-btn-3 ${isOpen ? "active" : ""}`}
      onClick={handleClick}>
        <span></span>
    </div>
  );
}
