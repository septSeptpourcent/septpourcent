import '../styles/menu-modal.scss'

import { Link } from 'react-router-dom';
import React from 'react';

const MenuModal = () =>{
    return (
        <>
            <div className="section">
                <div className="layer1">
                    <Link to="/equipe" className='link-menu-modal'>
                        <h3>Equipe</h3>
                    </Link>
                </div>
            </div>
            <div className="section">
                <div className="layer2">
                    <Link to="/competence" className='link-menu-modal'>
                            <h3>Compétence</h3>
                    </Link>
                </div>
            </div>
            <div className="section">
                <div className="layer3">
                    <Link to="/service" className='link-menu-modal'>
                        <h3>Nos services</h3>
                    </Link>
                </div>
            </div>
            <div className="section">
                <div className="layer4">
                    <Link to="/about" className='link-menu-modal'>
                        <h3>A propos</h3>
                    </Link>
                </div>
            </div>
            <div className="section">
                <div className="layer5">
                    <Link to="/contact" className='link-menu-modal'>
                        <h3>Nous contacter</h3>
                    </Link>
                </div>
            </div>
        </>
    )
}

export default MenuModal;