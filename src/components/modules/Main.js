import { Link } from "react-router-dom";
import Footer from "../layout/Footer";
import RemindContact from "../RemindContact";

import '../../styles/components/CompetenceSection.scss'
import '../../styles/components/ArticleSection.scss'
import SliderTeam from "../SliderTeam";
import Hero from "../Hero";
import ArrowDown from "../ArrowDown.js";

function Skill(props){
    return(
        <div className="skill_text" id="card">
            <div className="skill_data">
                <h5 className="skill_title">
                    {props.title}
                </h5>
                <p className="skill_p">
                    {props.text}
                </p>
            </div>
            <img className="skill_img" src={`./assets/skill/${props.img_src}.svg`} alt={`${props.img_alt}`}/>
        </div>
    )
}

function Article(props){
    return(
        <div className="article_text">
            <img className="article_img" src={`./assets/article/${props.img_src}.svg`} alt={`${props.img_alt}`}/>
            <div className="article_data">
                <h5 className="article_title">
                    {props.title}
                </h5>
                <p className="article_p">
                    {props.text}
                </p> 
            </div>
            <ul>
                    <li>
                        <img className="article_tag" src={`./assets/article/tags/${props.tag_src}.svg`} alt={`${props.tag_alt}`}/>
                    </li>
                    <li>
                        <img className="article_tag" src={`./assets/article/tags/${props.tag2_src}.svg`} alt={`${props.tag2_alt}`}/>
                    </li>
                </ul>
        </div>
    )
}

export default function Main(){
    return(
        <>
            <section className="section1">
                <Hero/>
            </section>

            <section className="section2">
                <div>
                    <h2>VOUS SOUHAITEZ CHOISIR VOTRE FUTURE AGENCE DE COMMUNICATION ?</h2>
                    <div className="arrow-down">
                        <ArrowDown/>
                    </div>
                </div>
            </section>

            <section className="section3">
                <div className="container-competence">
                    <div className="skills">
                        <Skill className="skill" title="Campagne emailing" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="email_campaign" img_alt="Campagne d'email"/>
                        <Skill className="skill" title="Marketing des réseaux sociaux" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="mobile_marketing" img_alt="Marketing des réseaux sociaux"/>
                        <Skill className="skill" title="SEA - Ads" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="online_ad" img_alt="SEA et ads"/>
                        <Skill className="skill" title="Développement web" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="web_development" img_alt="Dévelopemment web"/>
                        <Skill className="skill" title="SEO - Référencement" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="seo_campaign" img_alt="SEO et référencement"/>
                        <Skill className="skill"title="Audit Cybersécurité" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="security" img_alt="Cybersécurité et audit"/>
                    </div>
                </div>
            </section>

            <section className="section4">
                <h2>Notre équipe</h2>
                <div className="container-team">
                    <SliderTeam/>
                </div>
            </section>

            <section className="section5">
                <h2>Notre blog</h2>
                <div className="container-article">
                    <Link className="article_link">
                        <Article className="article" link="security" title="Nos audits cybersécurité" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="securite" img_alt="securité" tag_src="locked" tag_alt="locked icon" tag2_src="security" tag2_alt="security icon"/>
                    </Link>
                    <Link className="article_link">
                        <Article className="article" link="emailing" title="Nos campagnes emailing" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="emailing" img_alt="emailing" tag_src="email" tag_alt="email icon" tag2_src="mail" tag2_alt="mail icon"/>
                    </Link>
                    <Link className="article_link">
                        <Article className="article" link="social_network" title="Comment se lancer sur les réseaux sociaux ?" text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." img_src="reseaux_sociaux" img_alt="reseaux sociaux" tag_src="social" tag_alt="social icon" tag2_src="globalnetwork" tag2_alt="globalnetwork icon"/>
                    </Link>
                </div>
            </section>
            <RemindContact/>

            <Footer/>
        </>
    )
}