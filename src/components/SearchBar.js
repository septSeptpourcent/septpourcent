import React, { useState, useRef } from 'react';
import '../styles/search-bar.scss'
export default function SearchBar(){
    const inputRef = useRef(null);
    const [value, setValue] = useState('');

    const handleIconClick = () => {
        inputRef.current.focus();
    };
    
    return(
        <div className="searchbar-container">
            <svg   className="search-icon" 
                    onClick={handleIconClick} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor"
            >
                <path strokeLinecap="round" strokeLinejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
            </svg>
            <div className="form__group field">
                <input className="form__field" ref={inputRef} value={value} onChange={(event) => setValue(event.target.value)} onBlur={() => setValue('')} type="input"  placeholder="Name" name="name" id='name' required />
                <label form="name" className="form__label">RECHERCHER</label>
            </div>
        </div>
    )
}