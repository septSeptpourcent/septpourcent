import Lottie from "lottie-react";
import ArrowDownJSON from '../assets/img/arrowdown.json'

export default function ArrowDown(){
    return <Lottie animationData={ArrowDownJSON} />;
}