import MenuModal from "../components/MenuModal";
import Main from "../components/modules/Main";

import '../styles/home.scss'

export default function Home({ menu, handleClick }){
    

    return(
        <div className="app">
            <div className="container" style={{ display: 'inline-block' }}>
                <div className={`${menu ? '' : 'hidden'}`}>
                    <MenuModal />
                </div>
                <div className={`${menu ? 'hidden' : ''}`}>
                    <Main /> 
                </div>
            </div>
        </div>
    )
}